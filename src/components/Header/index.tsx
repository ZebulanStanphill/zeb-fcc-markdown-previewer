// External dependencies
import { createElement as h } from 'react';

// Internal dependencies
import './style.css';

export default function Header(): JSX.Element {
	return (
		<header className="zmp-header">
			<h1 className="zmp-header__h1">Zeb’s Markdown Previewer</h1>
		</header>
	);
}