// External dependencies
import { createElement as h, useState } from 'react';

// Project dependencies
import MarkdownPreview from '../MarkdownPreview';

// Internal dependencies
import './style.css';

const defaultStartingContent =
`Type some Markdown into this editor!
You can make text **strong** or *emphasize* it.
# You can also make headings
## Here is a level 2 heading
- here is a list
- with 2 items

Here is a [link to my website](https://zebulan.com).

Check out this \`inline code snippet\`
\`\`\`
Here is a block of code
taking up multiple lines
\`\`\`

> This is a blockquote!

Here is an image:

![example image](https://zebulan.com/wp-content/uploads/2018/08/zebulan-logo-00bbee-400.png)`;

export default function MarkdownEditor( { startingContent = defaultStartingContent } ): JSX.Element {
	const [ content, setContent ] = useState( startingContent );

	return (
		<div className="zmp-markdown-editor">
			<textarea
				className="zmp-markdown-editor__textarea"
				id="editor"
				onChange={ ( event ): void => setContent( event.target.value ) }
				value={ content }
			/>
			<MarkdownPreview content={ content } />
		</div>
	);
}