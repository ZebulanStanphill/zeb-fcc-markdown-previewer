// External dependencies
import { createElement as h } from 'react';

// Project component dependencies
import Header from '../Header';
import Footer from '../Footer';
import MarkdownEditor from '../MarkdownEditor';

// Internal dependencies
import './style.css';

export default function App(): JSX.Element {
	return (
		<div className="zmp">
			<Header />
			<MarkdownEditor />
			<Footer />
		</div>
	);
}