// External dependencies
import marked from 'marked';
import { createElement as h, memo } from 'react';

// Internal dependencies
import './style.css';

type Props = {
	content: string
}

export default memo( function MarkdownPreview( { content }: Props ): JSX.Element {
	return (
		<div
			className="zmp-markdown-preview"
			id="preview"
			dangerouslySetInnerHTML={
				{ __html: marked( content, { breaks: true, gfm: true } ) }
			}
		/>
	);
} );