// External dependencies
import { createElement as h } from 'react';
import { render } from 'react-dom';

// Project component dependencies
import App from './components/App';

render( <App />, document.getElementById( 'app-root' ) );